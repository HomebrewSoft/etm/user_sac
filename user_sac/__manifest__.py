{
    "name": "User SAC",
    "version": "13.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",
    "license": "LGPL-3",
    "depends": [
        "account",
        "sales_cube",
        "sale",
    ],
    "data": [
        # security
        # data
        # reports
        # views
        "views/account_move.xml",
        "views/sale_order.xml",
    ],
}
