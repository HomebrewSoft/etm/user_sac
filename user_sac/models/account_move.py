from odoo import _, api, fields, models


class AccountMove(models.Model):
    _inherit = "account.move"

    user_sac_id = fields.Many2one(
        related="partner_id.user_sac_id",
        store=True,
    )
