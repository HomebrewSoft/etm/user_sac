from odoo import _, api, fields, models


class SaleOrder(models.Model):
    _inherit = "sale.order"

    user_sac_id = fields.Many2one(
        related="partner_id.user_sac_id",
        stored=True,
    )
